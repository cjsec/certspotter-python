#!/usr/bin/python3
""" CertSpotter API query tool and [future] module

Day two of learning python and recreating my CertSpotter Perl
script in python.

Perl script is at: https://gitlab.com/cjsec/certspotter-perl
 
I've only been using python for two days... don't be surprised 
if some of this code is ugly or needs to go away.
"""

import certspotter
import logging
import sys
import argparse
import configparser

VERSION='0.6'
def init_argparse() -> argparse.ArgumentParser:

    parser = argparse.ArgumentParser(
        description="Query SSLMate's CertSpotter API"
    )

    parser.add_argument('domain', type=str, nargs='+',
            help="Domain(s) or sub-domain(s) for query"
    )

    parser.add_argument('--key', type=str, 
            help="your API Key - Overrides APIKEY in config"
    )

    parser.add_argument('-w', '--workdir', type=str, 
            help="work directory - Overrides WORKDIR in config"
    )

    parser.add_argument('-b', '--brief', action='store_true',
            help="brief listing of retrieved certificates "
    )

    parser.add_argument('--config', type=str, default='',
            help="get defaults from configuration file CONFIG"
    )

    parser.add_argument('-d', '--debug', action='count', default=0,
            help="debug level. Use once for INFO, twice for DEBUG."
    )

    parser.add_argument('--match', type=str, 
            help="exlude results that don't contain string MATCH"
    )

    parser.add_argument('-n', '--new', action='store_true', 
        help="include new certs issued since last recorded run"
    )

    parser.add_argument('-r','--record', action='store_true',
            help="record newest cert ID even without --new"
    )

    parser.add_argument('-s','--subs', action='store_true',
            help="retrieve all subdomains as well"
    )

    parser.add_argument('-t','--test', action='store_true',
            help="use test data instead of API call "
    )

    parser.add_argument('-x','--expires', nargs='?', type=int, const=30,
            help="only include results expiring within EXPIRES days"
    )

    parser.add_argument(
        '--version', action='version',
        version = f"{parser.prog} version {VERSION}"
    )

    return parser

parser = init_argparse()
args = parser.parse_args()

""" Set logging (debug) level and desination(s) """
if (args.debug>1):
    loglevel=logging.DEBUG
elif (args.debug>0):
    loglevel=logging.INFO
else:
    loglevel=logging.WARN

logFormatter = logging.Formatter('%(asctime)s %(levelname)s - %(message)s')
consoleFormatter = logging.Formatter('#LOG#: %(levelname)s - %(message)s')
logfileHandler = logging.FileHandler('localdebug.log')
consoleHandler = logging.StreamHandler()

logfileHandler.setFormatter(logFormatter)
consoleHandler.setFormatter(consoleFormatter)
logger = logging.getLogger(__name__)
logger.addHandler(logfileHandler)
logger.addHandler(consoleHandler)
logger.setLevel(level = loglevel)

logger.info(f"Logging level set: {logger}")
logger.debug(f"Commandline arguments: {args}")

if (args.config):
    logger.info(f"Configuration file selected {args.config}")
    # TODO - Confirm file exists, read configuration

""" Set API key in Authoriztion header """
# TODO - use config file or arg vs just passed arg
if (args.key):
    logger.info("CertSpotter API key set")
else:
    logger.warn("CertSpotter API key not provided")

if (args.expires):
    logger.info(f"Checking for certs expiring in {args.expires} days")
else:
    args.expires = 0

""" Override passed domain if --test mode """
if (args.test):
    logger.info("Using test data due to use of --test mode")

""" Clean passed domain name(s) - remove invalid chars and set lowercase """
logger.debug("Iterating through domains")
for domain in args.domain:
    logger.debug(f"Passed domain: {domain}")
    # TODO - add limiting search based on ID from saved domains list
    # TODO - Do we want to allow passing an ID from CLI?

    cl = certspotter.csquery(domain, test=args.test, subdomains=args.subs, key=args.key)
#    logger.info(f"Search domain: {sdomain}")
#    logger.debug(f"API URL = {cl.geturl()}")
    logger.debug(f"curl CMD = {cl.getCurlCmd()}")
    cl.fetch()
#    if (args.match):
#   TODO - Figure out best way to handle matches

    if args.expires > 0:
        cl.setExpiring(args.expires)
        certObjs = cl.expiring()
    else:
        certObjs = cl

    if args.brief:
        if args.expires > 0:
            #for cert in cl.expiring(args.expires):
            for cert in certObjs:
                print(f"{cert.getsubject()} ({cert.getnotafter_days()} days)")
        else:
            print(*cl.getAllSubjects(), sep = "\n")
    else: 
        if args.expires > 0:
            # List Expired Certs
            print(f"================Expiring CERTS - <{args.expires} days)==============================")
        else:
            # List ALL Certs
            print("=====================ALL CERTS===================================")
        for cert in certObjs:
            print(f"""==================================== 
            ID: {cert.getCertID()}
       Subject: {cert.getsubject()}
    Not Before: {cert.getnotbefore_text()}
     Not After: {cert.getnotafter_text()} ({cert.getnotafter_days()} days)
        Issuer: {cert.getissuer()}
        SAN(s): """, end="")
            print (*cert.getsans(), sep="\n          + ")

# For later - return highest ID
# return max(clist.keys(), key=int)
