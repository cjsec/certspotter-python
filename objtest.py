import certspotter

cl = certspotter.csquery('sslmate.com', test=True, subdomains=True, key='')
cl.fetch()
print (f"URL = {cl.geturl()}")

# List ALL Certs
print("=====================ALL CERTS===================================")
for o in cl.allCerts():
    print(f"""==================================== 
        ID: {o.getCertID()}
   Subject: {o.getsubject()}
Not Before: {o.getnotbefore_text()}
 Not After: {o.getnotafter_text()} ({o.getnotafter_days()} days)
    Issuer: {o.getissuer()}
    SAN(s): """, end="")
    print (*o.getsans(), sep="\n          + ")

print("===================BRIEF CERT LIST===============================")
print(*cl.getAllSubjects(), sep = "\n")

print("====================MATCHING CERTS===============================")
cl.setMatch('badge')
for o in cl.matches():
    print(f"""==================================== 
        ID: {o.getCertID()}
   Subject: {o.getsubject()}
Not Before: {o.getnotbefore_text()}
 Not After: {o.getnotafter_text()} ({o.getnotafter_days()} days)
    Issuer: {o.getissuer()}
    SAN(s): """, end="")
    print (*o.getsans(), sep="\n          + ")

print("===================EXPIRING CERTS================================")
cl.setExpiring(120)
for o in cl.expiring():
    cn = o.getsubject()
    for cert in cl.getCertsBySubject(cn):
        print (f"{cn} : Expiring in {cert.getnotafter_days()} days")
    print("------------------------")

print("===================FILTERED CERTS================================")
for o in cl.filtered():
    print(f"""==================================== 
        ID: {o.getCertID()}
   Subject: {o.getsubject()}
Not Before: {o.getnotbefore_text()}
 Not After: {o.getnotafter_text()} ({o.getnotafter_days()} days)
    Issuer: {o.getissuer()}
    SAN(s): """, end="")
    print (*o.getsans(), sep="\n          + ")

print("===================FILTERED CERTS (BRIEF)========================")
print(*sorted(set(cert.getsubject() for cert in cl.filtered())), sep = "\n")

