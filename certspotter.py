import json
import cstestdata
import base64
import requests
import re
import imp
if imp.util.find_spec("localtestdata") is not None:
    import localtestdata
from datetime import datetime
from cryptography import x509
from cryptography.hazmat.backends import default_backend

# TODO - Add logging/debugging

class csobj(object):
    def __init__(self, csrecord):
        # TODO -  Instead of relying direct JSON values, should parse and load values 
        #         Unless API/data changes aren't a concern
        self.__dict__ = csrecord
        self.certJson = json.dumps(csrecord, indent=4)
        cert=base64.b64decode(csrecord['cert']['data'])
        der=x509.load_der_x509_certificate(cert, default_backend())
        self.subject=der.subject.get_attributes_for_oid(x509.NameOID.COMMON_NAME)[0].value
        self.not_valid_before=der.not_valid_before
        self.not_valid_after=der.not_valid_after
        self.issuer=der.issuer.get_attributes_for_oid(x509.NameOID.COMMON_NAME)[0].value
        self.expire_days=(self.not_valid_after-datetime.now()).days
    def getJson(self):
        return self.certJson
    def getCertID(self):
        return self.id
    def getsans(self):
        return self.dns_names
    def getnotbefore(self):
        return self.not_valid_before
    def getnotbefore_text(self):
        return self.not_valid_before.strftime("%m/%d/%Y %H:%M")
    def getnotafter(self):
        return self.not_valid_after
    def getnotafter_text(self):
        return self.not_valid_after.strftime("%m/%d/%Y %H:%M")
    def getnotafter_days(self):
        return self.expire_days
    def getsubject(self):
        return self.subject
    def getissuer(self):
        return self.issuer
    def isexpiring(self, expdays):
        return (self.expire_days <= expdays)

# Master class for CS interaction 
# TODO - Change how 'match' and 'expiring' filters are done
# TODO -   Return all certs via allCerts() method and filtered certs via __iter__()?
# TODO -   OR return all via __iter__ and filtered via filtered() method?
# TODO -     Until decided, create both allCerts() and filtered() methods.
class csquery():
    def __init__(self,domain='',test=False,lastid=0,key='',subdomains=True,match='',expires=0):
        self.test = test
        self.searchdomain=self.__cleanDomain(domain)
        if test:
            if imp.util.find_spec("localtestdata") is not None:
                __testDomain = localtestdata.testdomain()
            else:
                __testDomain = cstestdata.testdomain()
            if not re.search(f"{__testDomain}$",self.searchdomain):
                # If the requested search host isn't part of test data, 
                # change the search name to match test data
                # (Essentially cosmetic.)
                self.searchdomain=__testDomain
        # TODO - Logger vs print
        # print (f"Domain set to {self.searchdomain}")
        self.ids = {}
        self.url =''
        self.key=key
        self.set_apikey(key)
        self.setMatch(match)
        self.setExpiring(expires)
        if (subdomains):
            self.subs = True
        else:
            self.subs = False
        # Expecting an int
        if isinstance(lastid, str):
            lastid = int(lastid)
        self.lastid = lastid
        self.__seturl()
    def append(self, __id, __cso):
        self.ids[__id] = __cso
    def __iter__(self):
        for __id in self.ids.keys():
            yield self.ids[__id]
    def certids(self):
        return self.ids.keys()
    def allCerts(self):
        for __id in self.ids.keys():
            yield self.ids[__id]
    def filtered(self):
        for __cert in self.matches():
            if __cert.isexpiring(self.expires):
                yield __cert
    def getAllSubjects(self):
        return sorted(set(__cert.getsubject() for __cert in self))
    def getCertsBySubject(self, __cn):
        for __cert in self:
            if __cert.getsubject() == __cn:
                yield __cert
    def setMatch(self, __matchText):
        __newMatchText=self.__cleanDomain(__matchText)
        self.matchText=re.sub('\.','\.',__newMatchText)
    def matches(self, __matchText=''):
        if __matchText:
            self.setMatch(__matchText)
        for __cert in self.allCerts():
            if any(re.search(self.matchText,san) for san in __cert.getsans()):
                yield __cert
    def setExpiring(self, __days=0):
        self.expires=int(__days)
    def expiring(self):
        for __cert in self.allCerts():
            if __cert.isexpiring(self.expires):
                yield __cert
    def __str__(self):
        return f"CertSpotter (sub)domain: '{self.searchdomain}'"
    def fetch(self):
        if (self.test):
            if imp.util.find_spec("localtestdata") is not None:
                __r = json.loads(localtestdata.testdata())
            else:
                __r = json.loads(cstestdata.testdata())
        else:
            try:
                __r = requests.get(self.url, headers=self.headers).json()
                # IF error code return, raise exception
                if 'code' in __r:
                    APIresultCode=__r['code']
                    APIresultMsg=__r['message']
                    raise ValueError(APIresultCode, APIresultMsg)
            except requests.exceptions.RequestException as e:
                # HTML, network, etc. errors will cause fail as well
                raise SystemExit(e)
                
        # For domain regex - match actual '.' and only at end of domain name
        __domregex=re.sub('\.','\.',self.searchdomain) + '$'

        # Read JSON data into objects
        for __i in __r:
            if self.test:
                # Since the API handles limiting results, we'll 
                # emulate that for test data as well
                __cert = csobj(__i)
                if int(__cert.getCertID()) > self.lastid:
                    if self.subs:
                        # Limit subdomain results to subdomains (duh)
                        if any(re.search(__domregex,__san) for __san in __cert.getsans()):
                            self.append(__i['id'],__cert)
                    else:
                        if self.searchdomain == self.searchdomain in __cert.getsans():
                            # Limit host search results to exact matches
                            # Just check sans since san should include the cn subject
                            self.append(__i['id'],__cert)
            else:
                self.append(__i['id'],csobj(__i))
    def __seturl(self):
        self.url=f"https://api.certspotter.com/v1/issuances?domain={self.searchdomain}&include_subdomains={self.subs}&expand=cert&expand=dns_names"
        if (self.lastid):
            self.url = self.url + f"&after={self.lastid}"
    def geturl(self):
        self.__seturl()
        return self.url
    def getCurlCmd(self):
        return f"curl -H \"Authorization: Bearer {self.key}\" '{self.geturl()}'"
    def search_subdomains(self, __searchSubs):
        if (__searchSubs):
            self.subs = True
        else:
            self.subs = False
        self.__seturl()
    def set_apikey(self, key=''):
        if (key):
            self.headers = {"Authorization": f"Bearer {key}"}
            return True
        else:
            # TODO - Use logger instead of print this
            #print("CertSpotter API key not provided")
            self.headers = ''
            return False
    def __cleanDomain(self,__domain):
        """ purge invalid characters from domain name.
        And set to lowercase for consitency """
        return re.sub('[^A-Za-z0-9\.=]','',__domain).lower()
