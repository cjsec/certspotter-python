# Set these values then run to build test data code
#
# YEAH, it would probably be better to just look for a data file
# that is downloaded using curl or other tool
# ... TODO - Save custom data to a file and forget this nonsense
domain = 'sslmate.com'
apikey = ''


#############
import certspotter
import json
cl = certspotter.csquery(domain, test=False, subdomains=True, key=apikey)
cl.fetch()

print (f"""# Custom test data

def testdomain():
    return '{domain}'
def testdata():
    return \"\"\"
[ """)

certcount=0
for cert in cl:
    if certcount>0:
        print (",")
    certcount += 1
    print(cert.getJson(), end='')

print ("\n]\n\"\"\"")

